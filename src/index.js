// add support for mobile :hover
document.addEventListener("touchstart", function(){}, true);

// shuffle isotope-gallery on initial load
var shuffleTimeout;

// ensure proper layout of isotope-gallery on initial load
document.querySelectorAll('isotope-gallery isotope-item image-overlay').forEach(function(imageOverlay){
	imageOverlay.addEventListener('onload',function(){
		var gallery = this.parentNode;
		while( gallery && gallery.tagName.toLowerCase() !== 'isotope-gallery' ){
			gallery = gallery.parentNode;
		}
		if( gallery ){
			gallery.layout();

			clearTimeout(shuffleTimeout);
			shuffleTimeout = setTimeout(function(){
				gallery.shuffle();
			}, 200);
		}
	});
});