class IsotopeGallery extends HTMLElement {
    
    get filters() {
        var value = this.getAttribute('filters');
        if( value ){
            return value.split(',');
        }
    }

    set filters( value ) {
        if( value ){
            if( typeof(value) === 'string' ){
                this.setAttribute('filters', value);
            } else {
                this.setAttribute('filters', value.join(','));
            }
        } else {
            this.removeAttribute('filters');
        }
    }

    get orFilters() {
        var value = this.getAttribute('or-filters');
        if( value ){
            return value.split(',');
        }
    }

    set orFilters( value ) {
        if( value ){
            if( typeof(value) === 'string' ){
                this.setAttribute('or-filters', value);
            } else {
                this.setAttribute('or-filters', value.join(','));
            }
        } else {
            this.removeAttribute('or-filters');
        }
    }

    static get observedAttributes() {
        return ['filters', 'or-filters'];
    }
    
    
    layout() {
    	if( this._isotope ){
    		this._isotope.layout();
    	}
    }


    shuffle() {
        if( this._isotope ){
            this._isotope.shuffle();
        }
    }

    
    connectedCallback() {
    	var self = this;
        self.style.display = 'block';

        customElements.whenDefined('isotope-item').then(function(){
        	self._isotope = new Isotope(self, {
                itemSelector: 'isotope-item',
                masonry: {
                    columnWidth: 32,
                    gutter: 10
                },
                filter: self._filterCallback()
            });
        	self.dispatchEvent(new Event('onload'));
        });
    }

    disconnectedCallback() {
        if( this._isotope ){
            this._isotope.destroy();
        }
    }

    attributeChangedCallback(attribute, oldValue, newValue) {
        if( this._isotope && ['filters', 'or-filters'].indexOf(attribute) != -1 ){
            this._isotope.arrange({
                filter: this._filterCallback()
            });
        }
    }

    _filterCallback() {
        var gallery = this;

        return function( item ){
            if( !gallery.filters && !gallery.orFilters ){
                return true;
            }
            let visibleAND = true;
            let visibleOR = true;

            if( gallery.filters && item.tags ){
                visibleAND = gallery.filters.reduce(function( visible, value ){
                    return visible && item.tags.indexOf( value ) != -1;
                }, true);
            }

            if( gallery.orFilters && item.tags ){
                visibleOR = gallery.orFilters.reduce(function( visible, value ){
                    return visible || item.tags.indexOf( value ) != -1;
                }, false);
            }

            return visibleAND && visibleOR;
        };
    }
}
window.customElements.define('isotope-gallery', IsotopeGallery);