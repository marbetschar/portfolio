class IsotopeFilter extends HTMLElement {

    constructor(){
        super();

        if( !this.gallery ){
            this.gallery = 'isotope-gallery';
        }

        var shadowRoot = this.attachShadow({mode: 'open'});
        shadowRoot.innerHTML = `<style>
        :host {
            cursor: pointer;
            opacity: 1.0;
        }
        :host(:hover) {
            opacity: 0.8;
        }
        :host([checked]) {
            opacity: 0.6;
        }
        </style>
        <slot></slot>`;
    }

    get isOr() {
        return this.hasAttribute('is-or');
    }

    set isOr( value ) {
        if( value ){
            this.setAttribute('is-or', '');
        } else {
            this.removeAttribute('is-or');
        }
    }

    get gallery() {
        var value = this.getAttribute('gallery');
        if( value ){
            return document.querySelector(value);
        }
    }

    set gallery( value ) {
        if( value ){
            this.setAttribute('gallery', value);
        } else {
            this.removeAttribute('gallery');
        }
    }

    get checked() {
        return this.hasAttribute('checked');
    }

    set checked( value ) {
        if( value ){
            this.setAttribute('checked', '');
        } else {
            this.removeAttribute('checked');
        }
    }

    get value() {
        return this.getAttribute('value');
    }

    set value( value ) {
        if( value ){
            this.setAttribute('value', value);
        } else {
            this.removeAttribute('value');
        }
    }

    static get observedAttributes() {
        return ['checked'];
    }

    attributeChangedCallback(attribute, oldValue, newValue) {
    	var self = this;
    	
        if( self.gallery && attribute === 'checked' ){
            if( self.checked ){
                var newFilters = [ self.value ];

                if( self.isOr ){
                    newFilters.push.apply(newFilters, self.gallery.orFilters);
                    self.gallery.orFilters = newFilters;
                } else {
                	self.gallery.filters = newFilters;

                    // make sure we are unchecking all no longer active filters
                    document.querySelectorAll('isotope-filter').forEach(function( filter ){
                        if( !filter.isOr && self.gallery === filter.gallery && self.gallery.filters.indexOf(filter.value) == -1 ){
                            filter.checked = false;
                        }
                    });
                }
            
            } else {
                var remove = function( filter ){ return filter !== self.value; }

                if( self.isOr && self.gallery.orFilters ){
                	self.gallery.orFilters = self.gallery.orFilters.filter(remove);
                }

                if( !self.isOr && self.gallery.filters ){
                	self.gallery.filters = self.gallery.filters.filter(remove);
                }
            }
        }
    }

    connectedCallback() {
    	var self = this;
    	
    	self.addEventListener('click', function(e){
    		self.checked = !self.checked;
        });
    }

    disconnectedCallback() {
        this.removeEventListener('click');
    }
}
window.customElements.define('isotope-filter', IsotopeFilter);