class IsotopeItem extends HTMLElement {
    get tags() {
        var value = this.getAttribute('tags');
        if( value ){
            return value.split(',');
        }
    }

    set tags( value ) {
        if( value ){
            if( typeof(value) === 'string' ){
                this.setAttribute('tags', value);
            } else {
                this.setAttribute('tags', value.join(','));
            }
        } else {
            this.removeAttribute('tags');
        }
    }
}
window.customElements.define('isotope-item', IsotopeItem);
