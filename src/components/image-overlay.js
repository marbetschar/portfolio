class ImageOverlay extends HTMLElement {
    constructor(){
        super();

        var shadowRoot = this.attachShadow({ mode: 'open' });
        shadowRoot.innerHTML = `<style>
        .content {
            position: relative;
            width: 100%;
            height: 100%;
            margin: auto;
            overflow: hidden;
        }
        .content .content-overlay {
            background: rgba(0,0,0,0.7);
            position: absolute;
            height: 100%;
            width: 100%;
            left: 0;
            top: 0;
            bottom: 0;
            right: 0;
            opacity: 0;
            -webkit-transition: all 0.4s ease-in-out 0s;
            -moz-transition: all 0.4s ease-in-out 0s;
            transition: all 0.4s ease-in-out 0s;
        }
        .content:hover .content-overlay{
            opacity: 1;
        }

        .content-image{
            display: block;
        }

        .content-details {
            position: absolute;
            text-align: center;
            width: 100%;
            top: 50%;
            left: 50%;
            opacity: 0;
            -webkit-transform: translate(-50%, -50%);
            -moz-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
            -webkit-transition: all 0.3s ease-in-out 0s;
            -moz-transition: all 0.3s ease-in-out 0s;
            transition: all 0.3s ease-in-out 0s;
        }

        .content:hover .content-details{
            top: 50%;
            left: 50%;
            opacity: 1;
        }

        .content-details *{
            color: #fff;
        }

        .fade-bottom{
            top: 80%;
        }

        .fade-top{
            top: 20%;
        }

        .fade-left{
            left: 20%;
        }

        .fade-right{
            left: 80%;
        }
        </style>

        <div class="content">
            <a>
                <div class="content-overlay"></div>
                <img class="content-image" />
                <div class="content-details">
                    <slot></slot>
                </div>
            </a>
        </div>`;
    }

    get title() {
        return this.getAttribute('title');
    }

    set title( title ) {
        if( title ){
            this.setAttribute('title', title);
        } else {
            this.removeAttribute('title');
        }
    }

    get href() {
        return this.getAttribute('href');
    }

    set href( href ) {
        if( href ){
            this.setAttribute('href', href);
        } else {
            this.removeAttribute('href');
        }
    }

    get target() {
        return this.getAttribute('target');
    }

    set target( target ) {
        if( target ){
            this.setAttribute('target', target);
        } else {
            this.removeAttribute('target');
        }
    }

    get src() {
        return this.getAttribute('src');
    }

    set src( src ) {
        if( src ){
            this.setAttribute('src', src);
        } else {
            this.removeAttribute('src');
        }
    }

    get alt() {
        return this.getAttribute('alt');
    }

    set alt( alt ) {
        if( alt ){
            this.setAttribute('alt', alt);
        } else {
            this.removeAttribute('alt');
        }
    }

    get fade() {
        var value = this.getAttribute('fade');
        if( value ){
            return value.split(',');
        }
    }

    set fade( value ) {
        if( value ){
            if( typeof(value) === 'string' ){
                this.setAttribute('fade', value);
            } else {
                this.setAttribute('fade', value.join(','));
            }
        } else {
            this.removeAttribute('fade');
        }
    }

    get width() {
        return this.getAttribute('width');
    }

    set width( value ) {
        if( value ){
            this.setAttribute('width', value);
        } else {
            this.removeAttribute('width');
        }
    }

    connectedCallback() {
    	var self = this;
    	
        var anchor = this.shadowRoot.querySelector('a');
        if( anchor ){
            if( this.href ){
                anchor.href = this.href;

                if( this.target ){
                    anchor.target = this.target;
                }
            }

            if( this.title ){
                anchor.title = this.title;
            }
        }

        var image = this.shadowRoot.querySelector('img');
        if( image ){
            image.src = this.src;

            if( this.alt ){
                image.alt = image.alt = this.alt;
            }

            if( this.width ){
                image.width = this.width;
            }
        }

        var content = this.shadowRoot.querySelector('.content-details');
        if( content && this.fade ){
            content.className = 'content-details fade-' + this.fade.join(' fade-');
        }
        
        imagesLoaded(this.shadowRoot, function(){
        	self.dispatchEvent(new Event('onload'));
        });
    }
}
window.customElements.define('image-overlay', ImageOverlay);