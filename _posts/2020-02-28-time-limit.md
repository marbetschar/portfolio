---
categories:
- Linux
- Open Source
title: Time Limit
excerpt: Desktop Timer App für elementary OS
tags:
- Vala + Gtk
size: '320'
image: "/images/Time-Limit.gif"
external_url: https://appcenter.elementary.io/com.github.marbetschar.time-limit/

---
