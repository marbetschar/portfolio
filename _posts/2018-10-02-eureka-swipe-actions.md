---
title: Eureka Swipe Actions
date: 2018-10-02 15:30:59 +0000
excerpt: Integration von Swipe Actions in das Formular-Framework "Eureka".
categories:
- iPhone
tags:
- Apple Swift
- Open Source
image: "/images/EurekaSwipeX.gif"
external_url: https://github.com/xmartlabs/Eureka/issues/105
size: 320

---
