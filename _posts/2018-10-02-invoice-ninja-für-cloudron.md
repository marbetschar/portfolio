---
title: Invoice Ninja ❤️ Cloudron
categories:
- Docker
- Linux
date: 2018-10-02 15:17:31 +0000
excerpt: Docker Paket für Invoice Ninja zur Installation auf Cloudron.
tags:
- Cloudron
- Open Source
- PHP
image: "/images/InvoiceNinja-for-Cloudron.png"
external_url: https://cloudron.io/store/com.invoiceninja.cloudronapp.html
size: 320

---
