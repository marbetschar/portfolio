---
title: Apple Erinnerungen ❤️ Analytics
categories:
- macOS
date: 2018-10-04 19:15:02 +0200
excerpt: Produktivität messen basierend auf Apple Erinnerungen. Anzeige als Widget
  im macOS Dashboard.
tags:
- Apple Swift
- HTML5 + CSS3 + JavaScript
size: 320
image: "/images/ReminderStats.jpg"
external_url: ''

---
