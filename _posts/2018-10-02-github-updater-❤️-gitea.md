---
title: GitHub Updater ❤️ Gitea
date: 2018-10-02 15:29:16 +0000
excerpt: Automatische Updates von WordPress Plugins / Themes über "GitHub Updater"
  direkt aus "Gitea" installieren.
categories:
- Browser
tags:
- PHP
- Open Source
- HTML5 + CSS3 + JavaScript
image: "/images/GitHubUpdater-for-Gitea.png"
external_url: https://github.com/afragen/github-updater/issues/640
size: 240

---
