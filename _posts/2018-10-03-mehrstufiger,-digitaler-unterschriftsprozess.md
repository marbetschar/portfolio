---
title: Mehrstufiger, digitaler Unterschriftsprozess
categories:
- Linux
- Browser
date: 2018-10-02 22:04:55 +0000
excerpt: für die Vermögensverwalter einer namhaften Bank.
tags:
- Java
- HTML5 + CSS3 + JavaScript
- REST API
size: 320
image: "/images/Unterschriftsprozess.png"
external_url: ''

---
