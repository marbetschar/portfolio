---
title: Eureka SplitRow
date: 2018-10-02 15:26:49 +0000
excerpt: Zwei Eingabefelder mithilfe des Formular-Frameworks "Eureka" nebeneinander
  platzieren.
categories:
- iPhone
tags:
- Apple Swift
- Open Source
image: "/images/Eureka-SplitRow.jpg"
external_url: https://github.com/EurekaCommunity/SplitRow
size: 320

---
