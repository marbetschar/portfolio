---
title: Metabase ❤️ Cloudron
categories:
- Docker
- Linux
date: 2018-10-02 21:34:30 +0000
excerpt: Docker Paket für die Installation von Metabase auf Cloudron.
tags:
- Cloudron
- Java
- Open Source
size: 320
image: "/images/Metabase-for-Cloudron.png"
external_url: https://git.cloudron.io/mandelkind/metabase-app

---
