module Npm
  def self.runBuild(site, payload)
    return if @processed
    system "npm run build" 
    @processed = true
  end
end

Jekyll::Hooks.register :site, :after_init do |site, payload|
  Npm.runBuild(site, payload)
end