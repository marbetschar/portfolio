<p align="center">
    <img src="images/Marco-Betschart-Portfolio.jpg" width="320" alt="Marco Betschart - Portfolio Website">
</p>

# Prerequisites

Jekyll must be installed:


```
$: gem install bundler jekyll
```

For details see [Jekyll docs](https://jekyllrb.com/docs/installation/).


# Install

```
$: git clone git@gitlab.com:marbetschar/portfolio.git
$: cd portfolio
$: bundle install
$: npm install
```

# Run

Simply execute the following command to start developing:

```
$: bundle exec jekyll serve
```

... or if you want to access the website from another machine than localhost:

```
$: bundle exec jekyll serve --host=0.0.0.0
```

# Known Issues

* Jekyll currently does not pick up changes in JavaScript files automatically. Therefore, if we make changes within the `src` directory, we need to rerun `bundle exec jekyll serve` manually.